from django.conf.urls import url, include
from .views import index, add_status, profil, booklist, getbook

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'add_status', add_status, name='add_status'),
    url(r'^profil', profil, name = 'profil'),
    url(r'^booklist', booklist, name = 'booklist'),
    url(r'^getbook', getbook, name = 'getbook'),
]