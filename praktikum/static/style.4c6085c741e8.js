$(document).ready(function() {
    $(".changetheme").click(function(){
      $(".accordion").css("background-color", "#FFE4C4");
      $(".container").css("color", "#000000");
      $("html").toggleClass("main");
      $("body").toggleClass("main");
      $("h1").css("font-style","italic");
      $("h2").css("font-style","italic");
      $("button").css("font-style","italic");
      $("tr").css("font-style","bold");
});


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
    });
}
});


var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}