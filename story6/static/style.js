$(document).ready(function() {
    $(".changetheme").click(function(){
      $(".accordion").css("background-color", "#FFE4C4");
      $(".container").css("color", "#000000");
      $("html").toggleClass("main");
      $("body").toggleClass("main");
      $("h1").css("font-style","italic");
      $("h2").css("font-style","italic");
      $("button").css("font-style","italic");
      $("tr").css("font-style","bold")
    });

    $.ajax({
    url: "/story6/getbook/",
    success: function(result){
      result = result.items
      var hdr = "<thead><tr><th>Judul</th><th>Penulis</th><th>Cover</th><th>Deskripsi</th><th></th></tr></thead>";
        $("#tables").append(hdr);
        $("#tables").append("<tbody>");
      for(i=0; i<result.length; i++){
        var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>" +"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>" + "</td><td>" + result[i].volumeInfo.description + "</td><td>" + "<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'countStar("+ "\""+result[i].id+"\"" +")'> <i class='fa fa-star'style = 'color : yellow'></i></button>"+"</td></tr>";
        $("#tables").append(tmp);
      }
      $("#tables").append("</tbody>");
    }
    
    });

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
});


var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

var counter = 0;
var data="";
function countStar(id){
  var star = $('#'+id).html();
  if(star.includes("gray")) {
    counter--;
    $('#'+id).html("<i class='fa fa-star' style = 'color : yellow'></i>");
    $("#checkFav").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " favorit");
  }
  else{
    counter++;
    $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
    if(counter == 0){
      $("#checkFav").html("<i class='fa fa-star'style = 'color : gray'></i>  Favorit saya");
    }else{
    $("#checkFav").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " favorit");
    }
  } 
}

$('#search').on("keyup", function(e){
    q = e.currentTarget.value
    console.log(q)
    $.ajax({
    url: "/story6/getbook/",
    success: function(result){
      result = result.items
      var hdr = "<thead><tr><th>Judul</th><th>Penulis</th><th>Cover</th><th>Deskripsi</th><th></th></tr></thead>";
        $("#tables").append(hdr);
        $("#tables").append("<tbody>");
      for(i=0; i<result.length; i++){
        var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>" +"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>" + "</td><td>" + result[i].volumeInfo.description + "</td><td>" + "<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'countStar("+ "\""+result[i].id+"\"" +")'> <i class='fa fa-star'style = 'color : yellow'></i></button>"+"</td></tr>";
        $("#tables").append(tmp);
      }
      $("#tables").append("</tbody>");
    }
});