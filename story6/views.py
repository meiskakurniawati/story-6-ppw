from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import status
from .forms import status_Form
import requests


# Create your views here.
response={}

def index(request):
    response['stats'] = status_Form
    response['status'] = status.objects.all()
    html = 'home.html'
    return render(request, html, response)

		
def add_status(request):
    form = status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        a = status(description=response['description'])
        a.save()
        return index(request)
    else:
        return index(request)

def profil(request):
    return render(request, 'profil.html')

def booklist(request):
    return render(request,'story9.html',response)

def getbook(request):
    q = request.GET.get('q','')
    API_BOOK = "https://www.googleapis.com/books/v1/volumes?q=quilting" + q
    book = requests.get(API_BOOK).json()
    return JsonResponse(book)
# Create your views here.

